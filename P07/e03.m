
[u, users] = read_data('u.data');
Nu = length(users); % Numero de utilizadores

Set = create_set(Nu, users, u);

%if exist("j.mat")
%  load("j.mat")
%else
J = min_hash(u, Set);
save "j.mat" J;
%end

%% Com base na distˆancia, determina pares com
%% distˆancia inferior a um limiar pr´e-definido
threshold =0.6 % limiar de decis˜ao

SimilarUsers = similar_users(Nu, J, users, threshold)