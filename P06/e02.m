n_elements = 14;
bm_size = n_elements * 4;

data = { "Portugal" ; 
         "Espanha" ; 
         "França" ; 
         "Alemanha" ; 
         "Reino Unido" ; 
         "Suiça" ; 
         "Itália" ; 
         "Holanda" ; 
         "Dinamarca" ; 
         "Turquia" ;
         "Estados Unidos" ; 
         "Canadá" ; 
         "México" ; 
         "Brasil" };
         
bloom_filter = create_bloom_filter(data, n_elements, bm_size);

data_to_check = { "Portugal" ;
                  "Espanha" ;
                  "Suiça" ;
                  "Holanda" ; 
                  "Estados Unidos" ; 
                  "Irlanda" ;
                  "África do Sul" ;
                  "Venezuela" ;
                  "Rússia" ;
                  "Cuba" ;
                  "Perú" ;
                  "Moçambique" ;
                  "Angola" };
                  
for(i=1:length(data_to_check))
  fprintf("%s -> %d\n", data_to_check{i}, belongs(data_to_check{i}, bloom_filter, n_elements, bm_size));
end