function cor = test_hash_2(n_elements, n)
  hash_test = zeros(n_elements, n);
  for(i=1:n_elements)
    s = string_generator(10, 10);
    for(j=1:n)
      hash_test(i, j) = mod(string2hash(s, j), 128);
    end
  end
  cor = corr(hash_test);
  imagesc(cor);
end