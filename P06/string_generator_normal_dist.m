function s = string_generator_normal_dist(e_length, var_length, pmf)
  % Generates a random string of characters with length between min_length and max_length
  f = cumsum(pmf);
  str_length = round(rand()*var_length+e_length); 
  s_ascii_codes = zeros(1, str_length);
  for(i=1:str_length)
    u = rand();
    letter_number = sum(u>f)+1;
    if(letter_number<=26) %min
      s_ascii_codes(i) = letter_number+96; % convert to ascii code
    else % mai
      s_ascii_codes(i) = letter_number+38; % convert to ascii code
    end
  end
  s_ascii_codes;
  s = char(s_ascii_codes);
end