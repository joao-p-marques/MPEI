
T = [ [0.7 0.2 0.3]; [0.2 0.3 0.3]; [0.1 0.5 0.4] ];

% Ex. B

X0 = [1 0 0]';
X2 = T^2 * X0;

P_Chuva = X2(2)


% Ex. C

data = zeros(9, 20);

for i = 1:20
  s = T^i;
  data(:, i) = s(:); 
end

figure(1);
plot(data');

% Ex. D

data = zeros(9, 20);

for i = 1:20
  s = T^i;
  data(:, i) = s(:); 

  if(i==1) 
    continue;
  end

  if(abs(data(:, i) .- data(:, i-1)) <= 10e-4)
    break;
  end
end

figure(2);
plot(data');
