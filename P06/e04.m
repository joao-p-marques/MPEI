n_elements = 1000;
str_length = 40;
bm_size = 8000;
n_elements_test = 10000;
K = 15;

alpha = ['A':'Z' 'a':'z' ];  

%  ficheiros a serem processados (do projecto Gutemberg
ficheiros={'pg21209.txt','pg26017.txt'};

% obter funcao massa de probabilidade (pmf em Ingles)
% pmfPT=pmfLetrasPT(ficheiros,alpha);

% visualizar funcao massa de probabilidade
% stem(pmfPT);

data = {};

for(i=1:n_elements)
  data{i} = string_generator_normal_dist(str_length, 0, pmfPT);
end

data_test = {};

for(i=1:n_elements_test)
  data_test{i} = string_generator_normal_dist(str_length, 0, pmfPT);
end

fprintf("False Positives:\n");
false_positives = zeros(1, 15);
for(k=1:K)
  bloom_filter = create_bloom_filter_n(data, n_elements, bm_size, k);
  for(i=1:n_elements_test)
    false_positives(k) += belongs_n(data_test{i}, bloom_filter, n_elements, bm_size, k);
  end
  fprintf("%d: %d\n", k, false_positives(k));
end

stem(1:15, false_positives);