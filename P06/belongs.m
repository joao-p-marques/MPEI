function eval = belongs(element, bloom_filter, n_elements, bm_size)
  eval = true;
  
  hash_code = mod(string2hash(element), bm_size)+1;
  if(bloom_filter(hash_code)==0) 
    eval=false;
  end
  hash_code = mod(hashstring(element), bm_size)+1;
  if(bloom_filter(hash_code)==0) 
    eval=false;
  end
  hash_code = mod(hashcode(element), bm_size)+1;
  if(bloom_filter(hash_code)==0) 
    eval=false;
  end
end