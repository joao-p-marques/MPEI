
T = [ [0.7 0.8]; [0.3 0.2] ];
x0 = [1 0]'; %Estado inicial

% T = 
%   Nao_Faltar  Faltar
%    0.70000   0.80000 Nao_Faltar
%    0.30000   0.20000 Faltar

% x0 =

%    1 Nao_Faltar
%    0 Faltar

%Ex. A - Se estiver presente na aula de quarta numa determinada semana, 
%qual a probabilidade de estar presente na aula de quarta da semana seguinte ?

x2 = (T^2)*x0; %2 transiçoes de estado (2 aulas por semana)
Prob_N_Faltar_A = x2(1) %Prob. de nao faltar

% Ex. B - Se nao estiver presente na aula de quarta numa determinada semana, qual a probabilidade de estar ˜
% presente na aula de quarta da semana seguinte ?

x0 = [0 1]'; %Estado inicial. Não esteve na aula
x2 = (T^2)*x0; %2 transiçoes de estado (2 aulas por semana)
Prob_N_Faltar_B = x2(1) %Prob. de nao faltar

% Ex. C - Sabendo que esteve presente na primeira aula, qual a probabilidade de estar na ultima aula, assu- ´
% mindo que o semestre tem exactamente 15 semanas de aulas e nao existem feriados?

x0 = [1 0]'; %Estado inicial
x30 = (T^30)*x0; %30 transiçoes de estado (2 aulas por semana)
Prob_N_Faltar_B = x30(1) %Prob. de nao faltar

% Ex. D - Represente num grafico a probabilidade de faltar a cada uma das 30 aulas, assumindo que a proba- ´
% bilidade de estar presente na primeira aula e de 85 %. 

x0 = [0.85 0.15]'; %Estado inicial
x = zeros(31, 1);
x(1) = 0.15;
for(i=2:31)
    x(i) = ((T^(i-1))*x0)(2); %30 transiçoes de estado (2 aulas por semana)
end
plot(x);