function J = min_hash(movies, users)
  Nm = length(movies);
  Nu = length(users);
  K = 100; %no. hash functions
  
  H = zeros(Nm, Nu); %vetor de codigos hash de cada filme
  for(m=1:Nm)
    for(k=1:K)
      H(m, k) = string2hash(m, k);
    end
  end
  
  A = zeros(Nu, K); %Vetor de assinaturas de cada utilizador
  for(u=1:Nu)
    A(u, :) = min(H(users{u}, :));
  end  
  
  J = zeros(Nu); %Vetor de indices de Jaccard
  for(n1=1:Nu)
    for(n2=1:Nu)
      J(n1, n2) = sum(A(n1, :) == A(n2, :)) / K;
    end
  end