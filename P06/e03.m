n_strings = 1000;
str_length = 40;
bm_size = 8000;

alpha = ['A':'Z' 'a':'z' ];  

%  ficheiros a serem processados (do projecto Gutemberg
ficheiros={'pg21209.txt','pg26017.txt'};

% obter funcao massa de probabilidade (pmf em Ingles)
pmfPT=pmfLetrasPT(ficheiros,alpha);

% visualizar funcao massa de probabilidade
% stem(pmfPT);

bloom_filter = zeros(1, bm_size);
data = {};

for(i=1:n_strings)
  data{i} = string_generator_normal_dist(str_length, 0, pmfPT);
end

bloom_filter = create_bloom_filter(data, n_strings, bm_size);

% Parte B

n_strings_test = 10000;

for(i=1:n_strings_test)
  s = string_generator_normal_dist(str_length, 0, pmfPT);
  test(i) = belongs(s, bloom_filter, n_strings, bm_size);
end

disp(sum(test)); %Nº de strings que pertencem ao conjunto inicial, segundo o bloom filter