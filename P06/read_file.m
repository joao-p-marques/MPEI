function words = read_file(file)
  f_content = fileread(file);
  words = strsplit(f_content, '[^a-zA-Z]', 'DelimiterType', 'RegularExpression');
end  