
T = [ [1/3 1/4 0]; [1/3 11/20 1/2]; [1/3 1/5 1/2] ];

% E. A - Confirme que se trata de uma matriz estocastica

all(T(:) >= 0) % == 1 ?
sum(T) % == 1 ?

% Ex. B - Crie o vector relativo ao estado inicial considerando que no total temos 90 alunos, o grupo A tem
% o dobro da soma dos outros dois e os grupos B e C tem o mesmo n ˆ umero de alunos. 

x0 = [60 15 15]'

% Ex. C - Quantos elementos integrarao cada grupo no fim da aula 30 considerando como estado inicial o ˜
% definido na al´ınea anterior?

x30 = (T^30)*x0

% Ex. D - Quantos elementos integrarao cada grupo no fim da aula 30 considerando que inicialmente se dis- ˜
% tribuiram os 90 alunos equitativamente pelos 3 grupos?

x0 = [30 30 30]';
x30 = (T^30)*x0