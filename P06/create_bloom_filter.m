function bm = create_bloom_filter(data, n_elements, bm_size)
  bm = zeros(1, bm_size);
  for(i=1:n_elements)
    hash_code = mod(string2hash(data{i}), bm_size)+1;
    bm(hash_code) = 1;
    hash_code = mod(hashstring(data{i}), bm_size)+1;
    bm(hash_code) = 1;
    hash_code = mod(hashcode(data{i}), bm_size)+1;
    bm(hash_code) = 1;
  end
end