function cor = test_hash(n_elements)
  hash_test = zeros(n_elements, 3); %test for 3 hash functions
  for(i=1:n_elements)
    s = string_generator(10, 10);
    hash_test(i, 1) = string2hash(s);
    hash_test(i, 2) = hashstring(s);
    hash_test(i, 3) = hashcode(s);
  end
  cor = corr(hash_test);