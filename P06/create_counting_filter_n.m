function counting_filter = create_counting_filter_n(data,n_elements, cf_size, k, w)
  counting_filter = zeros(1, cf_size);
  for(i=1:n_elements)
    for(j=1:k)
      hash_code = mod(string2hash(data{i}, j), cf_size)+1;
      counting_filter(hash_code)++;
    end
  end
end