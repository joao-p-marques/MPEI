function [movie_data, users_data] = read_data(file)
  udata=load(file); % Carrega o ficheiro dos dados dos filmes
  
  movie_data=udata(1:end,1:2); 
  clear udata;
  
  users_data = unique(movie_data(:,1)); % Extrai os IDs dos utilizadores
end