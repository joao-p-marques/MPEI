function n = n_occurs(element, counting_filter, cf_size, k)
  n = 6000;
  for(i=1:k)
    hash_code = mod(string2hash(element, i), cf_size)+1;
    if(n>counting_filter(hash_code))
      n = counting_filter(hash_code);
    end
  end
end