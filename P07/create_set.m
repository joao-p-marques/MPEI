function movies_set = create_set(Nu, users, u)
  % Constroi a lista de filmes para cada utilizador
  movies_set = cell(Nu,1); % Usa celulas
  for n = 1:Nu, % Para cada utilizador
    % Obtem os filmes de cada um
    ind = find(u(:,1) == users(n));
    % E guarda num array. Usa celulas porque utilizador tem um numero
    % diferente de filmes. Se fossem iguais podia ser um array
    movies_set{n} = [movies_set{n} u(ind,2)];
  end
end