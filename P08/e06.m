
H = [ [0.8 0 0.3 0]; [ 0.2 0.9 .02 0]; [0 0.1 0.4 0]; [ 0 0 0.1 1] ]

% Ex. B
fprintf("B)\n");

X0 = [1 0 0 0]';

X1000 = H^1000 * X0;
P_2 = X1000(2)            %Valor pequeno devido a elemento absorvente

% Ex. C
fprintf("C)\n");

disp('1 passo: ')
H

disp('2 passos: ')
H^2

disp('10 passos: ')
H^10

disp('100 passos: ')
H^100

% Ex. D 
fprintf("D)\n");

Q = H(1:3,1:3)

% Ex. E
fprintf("E)\n");
  
F = inv(eye(3)-Q)

% Ex. F
fprintf("F)\n");

T = sum(F);
T(1)
T(2)
T(3)

% Ex. G
% Igual a F porque 4 é estado absorvente

% Ex. H
fprintf("H)\n");

H = [ [0.8 0 0.3 0]; [ 0.2 0.9 .02 0]; [0 0.1 0.4 0]; [ 0 0 0.01 1] ]

Q = H(1:3,1:3)
F = inv(eye(3)-Q)
T = sum(F);
T(1)
T(2)
T(3)

% Ex. I

% no ficheiro e07_sim.m
