function data = initialize_data(n_elements)
  data = {};
  for(i=1:n_elements)
    x = string_generator(3, 20); % Randomly generate string
    data(i) = x;
  end
end