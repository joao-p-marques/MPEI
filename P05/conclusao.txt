
-> S função hash consegui espalhar as várias strings pelo array.
Na 1a e 2a versão dos exercícios, vemos pelo histograma que a maior parte dos elementos do array ficaram com 1 elemento ou sem nenhum.
-> Contudo, alguns deles ficaram com 2, 3, 4, ou até 5 strings para o mesmo hash_code.
-> Apesar disso, conseguimos uma distribuição uniforme pelos elementos do array. Uma prova disso é que o valor esperado para o número de strings por chave é 0.8.
-> Ao alterar a funçao que usamos para gerar o hash code, obtemos valores ligeiramente diferentes mas muito parecidos, e igualmente bem distribuídos pelo array.