
[u, users] = read_data('u.data');
Nu = length(users); % Numero de utilizadores

new_users = zeros(100, 1);
for(i=1:100)
  usr = randi([1 length(users)], 1, 1);
  if(sum(users(usr) == new_users) != 0)
    i=i-1;
    continue;
  end
  new_users(i) = users(usr);
end
Nu = 100;

Set = create_set(Nu, new_users, u);

if exist("j.mat")
  load("j.mat")
else
  J = calc_dist(Nu, Set);
  save "j.mat" J;
end

%% Com base na distˆancia, determina pares com
%% distˆancia inferior a um limiar pr´e-definido
threshold =0.6 % limiar de decis˜ao

SimilarUsers = similar_users(Nu, J, new_users, threshold)