function bm = create_bloom_filter_n(data, n_elements, bm_size, k)
  bm = zeros(1, bm_size);
  for(i=1:n_elements)
    for(j=1:k)
      hash_code = mod(string2hash(data{i}, j), bm_size)+1;
      bm(hash_code) = 1;
    end
  end
end