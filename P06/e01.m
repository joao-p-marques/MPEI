n_elements = 1000;
bm_size = 4000;

data = initialize_data(n_elements);
bloom_filter = create_bloom_filter(data, n_elements, bm_size);

% Check elements that belong to the array
belongs(data{1}, bloom_filter, n_elements, bm_size)
belongs(data{3}, bloom_filter, n_elements, bm_size)
belongs(data{5}, bloom_filter, n_elements, bm_size)

printf "\n"
% Check elements that do not belong to the array
belongs("ola", bloom_filter, n_elements, bm_size)
belongs("aveiro", bloom_filter, n_elements, bm_size)
belongs("qualquercoisa", bloom_filter, n_elements, bm_size)

% Test hash_functions
disp(test_hash(1e4));