k = 15; % Number of hash functions to use
w = 100;

file='book1.txt';
words_book = read_file(file);

n_elements = length(words_book);
counting_filter_size = n_elements * 8;

counting_filter = create_counting_filter_n(words_book, n_elements, counting_filter_size, k, w);

% disp(sum(counting_filter'));

words_book = unique(words_book);

n_words_test = length(words_book);
file = fopen("results_e06.txt", "w");
fprintf(file, "Word occurs:\n");
for(i=1:n_words_test)
  fprintf(file, "%s: %d\n", words_book{i}, n_occurs(words_book{i}, counting_filter, counting_filter_size, k));
end
fclose(file);