k = 15; % Number of hash functions to use

files={'book1.txt','book2.txt'};

words_book1 = unique(read_file(files{1}));
words_book2 = unique(read_file(files{2}));

n_elements_1 = length(words_book1);
n_elements_2 = length(words_book2);

bloom_filter_1_size = n_elements_1 * 8;
% bloom_filter_2_size = n_elements_2 * 8; % Not used
 
bloom_filter_1 = create_bloom_filter_n(words_book1, n_elements_1, bloom_filter_1_size, k);

different_words = {};
dw_c = 1;

for(i=1:n_elements_2)
  if(!belongs_n(words_book2{i}, bloom_filter_1, n_elements_1, bloom_filter_1_size, k))
    different_words{dw_c} = words_book2{i};
    dw_c++;
  else
    % fprintf("Common word! : %s\n",  words_book2{i});
  end
end

fprintf("Number of different words: %d\n", dw_c);