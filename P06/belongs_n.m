function eval = belongs_n(element, bloom_filter, n_elements, bm_size, n)
  eval = true;
  for(k=1:n)
    hash_code = mod(string2hash(element, k), bm_size)+1;
    if(bloom_filter(hash_code)==0) 
      eval=false;
    end
  end
end