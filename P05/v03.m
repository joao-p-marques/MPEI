%  ficheiros a serem processados (do projecto Gutemberg
ficheiros={'pg21209.txt','pg26017.txt'};

% obter funcao massa de probabilidade (pmf em Ingles)
pmfPT=pmfLetrasPT(ficheiros,alpha);

%  visualizar funcao massa de probabilidade
% stem(pmfPT);

n_strings = 1000;
factor = 0.8;
n_elements = 1000/0.8;

hash_table_sim = zeros(1, n_elements);

for(i=1:n_strings)
  str = string_generator_normal_dist(10, 5, pmfPT);
  % Em vez de usar a funcao string2hash(), usar a hashcode()
  hash_code = mod(hashcode(str), n_elements);
  hash_table_sim(hash_code+1) = hash_table_sim(hash_code+1) + 1;
end

figure(1);
plot(hash_table_sim);
legend('Distribuiçao de palavras pela hash table');
xlabel('Código hash');
ylabel('Nº de palavras');

figure(2);
hist(hash_table_sim, 0:max(hash_table_sim));
legend('Histograma');
xlabel('Nº palavras');
ylabel('Códigos Hash');

p_x = zeros(1, max(hash_table_sim)+1);
e_x = 0;
e_x2 = 0;
for(x=0:length(p_x))
  p_x(x+1) = sum(hash_table_sim==x) / n_elements;
  e_x += x*p_x(x+1);
  e_x2 += (x^2)*p_x(x+1);
end
p_x
e_x
var_x = e_x2 - e_x^2

figure(3);
stem(p_x);
legend('Distribuiçao da variável aleatória X');