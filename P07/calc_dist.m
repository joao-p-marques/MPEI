function J = calc_dist(Nu, Set)
  %% Calcula a distˆancia de Jaccard entre todos os pares pela definic¸˜ao.
  J=zeros(Nu); % array para guardar distˆancias
  h=waitbar(0,'Calculating');
  tic;
  for n1=1:Nu,
    waitbar(n1/Nu,h);
    for n2=n1+1:Nu,
      J(n1, n2) = 1 - length(intersect(Set{n1}, Set{n2}))/length(union(Set{n1}, Set{n2}));
    end
  end
  delete (h)
  elapsed_time = toc;
  fprintf("Elapsed time (Jaccard): %f\n", elapsed_time);
end